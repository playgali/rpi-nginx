FROM playgali/alpine-qemu 
MAINTAINER Galileo Martinez "playgali@gmail.com"
ENV REFRESHED_AT 2017-Oct-21

RUN [ "cross-build-start" ]

RUN apk add --no-cache nginx

RUN [ "cross-build-end" ]

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
